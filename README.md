# ac-computadora
```mermaid
graph TD
    A[John Von Neuman] -->|nace en| B(Budapest, 1903)
    B --> C[estudio en la universidad de Budapest, 1921 - 1926]
    C -->|Estudio| D[y se doctoro en martematicas]
    D -->|En| E[Escuela Politecnica Federal Zurich, 1925]
    E -->|donde| F[se licencio en Ingenieria Quimica]
    B --> G[Propuso adopcion del bit como unidad de medida de memoria]
    B --> H[Fue un genio en las matematicas]
    G -->|La arquitectura que propuso| I[CPU]
    I --> M[nucleo central del computador]
    M --> N[Realiza operaciones basicas]
    M --> O[Gestiona funcionamiento del resto de componentes]
    G -->|La arquitectura que propuso| J[Memoria Principal]
    J --> P[se almacena]
    P --> Q[Datos]
    P --> R[Instrucciones]
    G -->|La arquitectura que propuso| K[Buses]
    K --> S[Permite comunicacion entre los distintos bloques del sistema]
    G -->|La arquitectura que propuso| L[Perifericos]
    L --> T[Se encarga de]
    T --> U[Tomar datos]
    U --> V[mostrar salida]
    T --> W[Comunicarse con otros sistemas]
```
