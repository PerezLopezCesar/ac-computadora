# ac-computadora
```mermaid
graph TD
    A[Supercomputadoras en México] -->|hace| B[51 años, comienza la era de la computacion en Mexico]
    B --> |para| C[Mediados de los 80S]
    C --> |se normaliza su uso| D[En oficinas y hogares]
    B --> |en 1991| E[La UNAM adquiere CRAY 432]
    E --> |la| G[La primer supercomputadora en America Latina]
    G --> H[Equivalente a 2000 computadoras de oficina]
    G --> |su uso| I[se enfoco para la ciencia Mexicana]
    G --> J[A principios del nuevo siglo]
    J --> |se sustituye| K[por KanBalan]
    K --> L[Equivalente a 1300 computadoras de oficina]
    L --> M[capaz de realizar 7 Billones de operaciones por segundo]
    K --> N[Calculos que tomarian años se hacen en samanas]
    K --> |se uso| O[para proyectos de investigacion]
    B --> |en 1958| F[La UNAM tuvo la primer computadora en toda LATAM]
    F --> |se uso| P[en la investigacioin cientifica]
    F --> |fue| Q[la IBM60]
    Q --> |capaz| R[de realizar 1000 operaciones aritmeticas por segundo]
```
